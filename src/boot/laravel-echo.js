import Echo from 'laravel-echo'
import { LocalStorage } from 'quasar'

window.io = require('socket.io-client')

window.Pusher = require('pusher-js');


const echo = new Echo({

  broadcaster: 'pusher',
  key: 'myappkey',
  cluster: 'mt1',
  forceTLS: false, // true p/ https
  wsHost: 'chat-rt_bk.test',
  wsPort: 6001,
  authEndpoint: 'http://chat-rt_bk.test/broadcasting/auth',
  // auth: {
  //   headers: {
  //     Authorization: LocalStorage.has('dados_login') ? 'Bearer ' + LocalStorage.getItem('dados_login').jwt : null,
  //     // 'Authorization': 'Bearer ******'
  //   }
  // }
})

export default ({Vue}) => {
  Vue.prototype.$echo = echo
}

