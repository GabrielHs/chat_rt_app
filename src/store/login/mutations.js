export function SET_LOGIN(state, resultado) {
  state.id = resultado.id,
    state.name = resultado.name,
    state.email = resultado.email,
    state.created_at = resultado.created_at,
    state.updated_at = resultado.updated_at,
    state.jwt = resultado.jwt,
    state.image =  resultado.image
}

export function UPDATE_LOGIN(state, resultado) {

  state.id = resultado.id,
    state.name = resultado.name,
    state.email = resultado.email,
    state.created_at = resultado.created_at,
    state.updated_at = resultado.updated_at,
    state.jwt = resultado.jwt,
    state.image = resultado.image
}

export function SET_ENCERRA_LOGIN(state, resultado) {

  state.id = 0,
    state.name = "",
    state.email = "",
    state.created_at = "",
    state.updated_at = "",
    state.jwt = "",
    state.image = ""


  // state = resultado
}
