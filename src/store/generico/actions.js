const setTitulo = ({ commit }, titulo) => {
  commit("SET_TITULO", titulo);
};


export { setTitulo };
