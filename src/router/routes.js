const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '', component: () => import('pages/Index.vue')
      },
      {
        path: 'usuarios', component: () => import('pages/Users.vue')
      },
      {
        path: '/login',
        component: () => import('pages/Auth.vue')
      },
      {
        path: '/chat/:user_id/',
        component: () => import('pages/Chat.vue')
      }
    ],
  },
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
