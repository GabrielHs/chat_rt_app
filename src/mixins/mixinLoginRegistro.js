import Vue from "vue";
import {Loading, LocalStorage, Notify, QSpinnerGears} from "quasar";
import {mapActions, mapState} from "vuex";


const mixinLoginRegistro = {
  methods: {
    ...mapActions("generico", ["setTitulo"]),
    ...mapActions("login", [
      "setLogin",
      "setEncerraLogin",
    ]),
    tentaLogin() {
      let path = this.$route.path;
      if (path === "/login") {
        if (LocalStorage.has("dados_login")) {
          let dados = LocalStorage.getItem("dados_login");
          this.salvaSessao(dados);
        } else {
          this.setTitulo("Autenticar");
          this.$router.push("/login", () => {
          });
        }
      }
      // Login via token
      let dados_login = this.login;
      if (dados_login.jwt) {
        this.setLogin(dados_login);
      } else if (LocalStorage.has("dados_login")) {
        let dados = LocalStorage.getItem("dados_login");
        this.salvaSessao(dados);
      } else {
        this.setTitulo("Autenticar");


        this.$router.push("/login", () => {
        });
      }
    },
    salvaSessao(dados_login) {
      LocalStorage.set("dados_login", dados_login);
      this.setLogin(dados_login);

      let token = dados_login.jwt || LocalStorage.getItem('dados_login').jwt
      this.$axios.defaults.headers.common["Authorization"] =
        "Bearer " + token;

      this.$echo.connector.options.auth.headers.Authorization = 'Bearer ' + LocalStorage.getItem('dados_login').jwt; // socket.io

      this.$echo.connector.pusher.config.auth.headers['Authorization'] = 'Bearer ' + LocalStorage.getItem('dados_login').jwt;

      // this.setTitulo("Gzap");

      this.$router.push("/usuarios", () => {
      });
    },
    encerraSessao() {
      this.$echo.connector.pusher.config.auth.headers['Authorization'] = 'Bearer ' + LocalStorage.getItem('dados_login').jwt;
     // console.log(this.$echo)
      this.$echo.leave(`presence-online`);
      this.$axios.get(`${process.env.API}logout`).finally(e => {
        LocalStorage.remove("dados_login");

        this.setEncerraLogin();
        this.setTitulo("Autenticar");
        this.$router.push("/login", () => {
        });
      })

    },
    submit(dados, tipo = "login") {
      Loading.show({
        message: "Recebendo informações",
        spinner: QSpinnerGears
      });
      if (tipo === "login") {
        Vue.prototype.$axios
          .post(`${process.env.API}login`, {
            email: dados.email,
            password: dados.senha
          })
          .then(res => {

            let body = res.data;
            if (body) {
              this.setLogin(body);
              LocalStorage.set("dados_login", body);

              if (body.jwt) {
                this.$axios.defaults.headers.common["Authorization"] =
                  "Bearer " + body.jwt;
              }

              this.$router.push("/usuarios", () => {
              });

            } else {
              Notify.create({
                type: "info",
                message: res.data
              });
              this.$router.push("/login", () => {
              });
            }
          })
          .catch(err => {
            switch (Number(err.response?.status)) {
              case 400:
                Notify.create({
                  type: "warning",
                  message: err.response.data
                });
                break;

              case 500:
                Notify.create({
                  type: "negative",
                  message: "Erro no servidor"
                });
                break;
            }
          })
          .finally(() => {
            Loading.hide();
          });
      } else {
        let formData = new FormData();
        formData.append('image', dados.image);
        formData.append('name', dados.name);
        formData.append('email', dados.email);
        formData.append('password', dados.senha);
        Vue.prototype.$axios
          .post(`${process.env.API}register`, formData, {
            headers: {'content-type': 'multipart/form-data'}
          })
          .then(res => {

            let body = res.data;
            if (body) {

              Notify.create({
                type: "info",
                message: 'Usuário criado com sucesso'
              });

              this.$router.push("/login", () => {
              });
            } else {
              Notify.create({
                type: "info",
                message: res.data
              });

            }
          })
          .catch(err => {
            switch (Number(err.response?.status)) {
              case 400:
                Notify.create({
                  type: "warning",
                  message: err.response.data
                });
                break;

              case 500:
                Notify.create({
                  type: "negative",
                  message: "Erro no servidor"
                });
                break;
            }
          })
          .finally(() => {
            Loading.hide();
            this.$router.push("/login", () => {
            });
          });
      }
    }
  },
  computed: {
    $user_id() {
      return !!this.login ? parseInt(this.login.id) : 0;
    },
    ...mapState("login", ["login"]),
    ...mapState("generico", ["generico"])
  },
  beforeRouteEnter(to, from, next) {
    next(vm => {
      vm.tentaLogin();
    });
  }

};


export default mixinLoginRegistro;
