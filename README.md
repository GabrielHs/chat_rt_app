# Quasar App (gzap)

projeto chat real time com laravel e quasar


![Alt text](./f1.PNG)
![Alt text](./f4.png)
![Alt text](./f3.png)
![Alt text](./f2.png)


## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```


### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).
